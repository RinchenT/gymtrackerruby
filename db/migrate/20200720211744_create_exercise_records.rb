class CreateExerciseRecords < ActiveRecord::Migration[5.2]
  def change
    create_table :exercise_records do |t|
      t.integer :exercise_id
      t.string :name
      t.string :before
      t.string :after
      t.timestamps
    end
  end
end
