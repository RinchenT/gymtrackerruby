class AddMetricToExerciseTable < ActiveRecord::Migration[5.2]
  def change
    add_column :exercises, :weight_metric, :string
    add_column :exercises, :time_metric, :string
  end
end
