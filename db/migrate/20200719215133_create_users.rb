class CreateUsers < ActiveRecord::Migration[5.2]
  def change
    create_table :users do |t|
      t.string :email, null: false
      t.string :password_digest, null: false
      t.string :first_name, null: false
      t.string :last_name, null: false
      t.string :height
      t.string :height_metric
      t.string :weight
      t.string :weight_metric
      t.timestamps
    end
  end
end
