class UpdateUserTable < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :workout_stage, :integer
  end
end
