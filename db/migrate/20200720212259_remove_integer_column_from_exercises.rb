class RemoveIntegerColumnFromExercises < ActiveRecord::Migration[5.2]
  def change
    remove_column :exercises, :integer
  end
end
