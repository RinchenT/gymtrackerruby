class CreateExercises < ActiveRecord::Migration[5.2]
  def change
    create_table :exercises do |t|
      t.string :name
      t.integer :reps
      t.integer :sets
      t.integer :integer
      t.float :weight
      t.float :time
      t.timestamps
    end
  end
end
