require 'test_helper'

class ExercisesControllerTest < ActionDispatch::IntegrationTest
  test "should get new" do
    get exercises_new_url
    assert_response :success
  end

  test "should get edit" do
    get exercises_edit_url
    assert_response :success
  end

end
