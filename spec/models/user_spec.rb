require 'rails_helper'

RSpec.describe User, type: :model do
  context "creating a new valid user" do
    it "should successfully create user account" do
      user = User.create!(email: "gym@app.com", first_name: "Gym", last_name: "App", password: "12345")
      expect(user).to eq (User.first)
    end
  end

  context "creating a user with existing email" do
    it "should not create user account" do
      
    end
  end
end
