Rails.application.routes.draw do
  get 'users/edit'
  root :to => 'home#index'
  resources :users
  resources :workouts
  resources :exercises
  get 'complete_workout', to: 'workout_session#complete_workout'
  get 'workout_session', to: 'workout_session#index'
  get 'dashboard', to: 'dashboard#index'
  get 'signup', to: 'users#new'
  get 'login', to: 'sessions#new'
  post 'login', to: 'sessions#create'
  get 'welcome', to: 'sessions#welcome'
end
