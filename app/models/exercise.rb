class Exercise < ApplicationRecord
  belongs_to :workout
  has_many :exercise_records
  
  after_update :create_record

  enum weight_metric: [:feet, :cm]
  enum time_metric: [:seconds, :minutes]

  def create_record
    check_changes = ["weight", "reps", "sets", "time"]
    value_changed = ""
    check_changes.map { |value| previous_changes[value] && value_changed = value}

    return unless value_changed.present?

    ExerciseRecord.create(exercise_id: id, name: value_changed, before: previous_changes[value_changed][0], after: previous_changes[value_changed][1])
  end
end
