class Workout < ApplicationRecord
  belongs_to :user
  has_many :exercises

  after_create :set_user_workout_stage
  before_destroy :remove_user_workout_stage

  def set_user_workout_stage
     return if user.workout_stage.present?

     user.workout_stage = rank
     user.save!
  end

  def remove_user_workout_stage
    return unless user.workout_stage === rank
    user.workouts.size > 0 && user.workout_stage = user.workouts.first
  end
end
