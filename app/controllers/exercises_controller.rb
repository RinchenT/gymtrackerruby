class ExercisesController < ApplicationController
  before_action :set_exercise, only: [:update, :destroy]
  def index

  end

  def new
    @exercise = Exercise.new
  end

  def edit
    @exercise = Exercise.find(params[:id])
  end

  def create
    @exercise = Exercise.new(exercise_params)

    respond_to do |format|
      if @exercise.save
        format.html { redirect_to edit_workout_path(@exercise.workout_id), notice: 'Exercise was successfully created.' }
        format.json { render :show, status: :created, location: edit_workout_path(@exercise.workout_id) }
      else
        format.html { render :new }
        format.json { render json: @exercise.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @exercise.update(exercise_params)
        format.html { redirect_to request.referrer, notice: 'Exercise was successfully updated.' }
        format.json { render :show, status: :ok, location: request.referrer }
      else
        format.html { render :edit }
        format.json { render json: @exercise.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @exercise.destroy
    redirect_to request.referrer
  end

private

  def set_exercise
    @exercise = Exercise.find(params[:id])
  end

  def exercise_params
    params.require(:exercise).permit(:name, :reps, :weight, :sets, :time, :workout_id)
  end
end
