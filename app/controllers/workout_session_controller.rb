class WorkoutSessionController < ApplicationController
  before_action :set_workout_session

  def complete_workout
    user = @workout.user

    return unless user.workouts.size > 1

    if user.workouts.where(rank: @workout.rank + 1).any?
      user.workout_stage = @workout.rank + 1
    else
      user.workout_stage = user.workouts.order(rank: :asc).first.rank
    end

    user.save

    redirect_to dashboard_path
  end
private

  def set_workout_session
    @workout = current_user.workouts.where(rank: current_user.workout_stage).first
  end
end
