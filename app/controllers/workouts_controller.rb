class WorkoutsController < ApplicationController
  before_action :set_workout, only: [:update]

  def index
    @workouts = current_user.workouts
  end

  def new
    @workout = Workout.new
  end

  def edit
    @workout = Workout.find(params[:id])
    @exercise = Exercise.new
  end

  def create
    @workout = Workout.new(workout_params)
    @workout.user_id = current_user.id

    respond_to do |format|
      if @workout.save
        format.html { redirect_to workouts_path, notice: 'Workout was successfully created.' }
        format.json { render :show, status: :created, location: workouts_path }
      else
        format.html { render :new }
        format.json { render json: @workout.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @workout.update(workout_params)
        format.html { redirect_to workouts_path, notice: 'Workout was successfully updated.' }
        format.json { render :show, status: :ok, location: workouts_path }
      else
        format.html { render :edit }
        format.json { render json: @workout.errors, status: :unprocessable_entity }
      end
    end
  end

private
  def set_workout
    @workout = Workout.find(params[:id])
  end
  def workout_params
    params.require(:workout).permit(:name, :rank)
  end
end
